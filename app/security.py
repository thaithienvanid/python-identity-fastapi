import json

from jose import jwk

from app.config import settings

token_key = jwk.construct(key_data=json.loads(settings.sec.key))
