import bcrypt
import sqlalchemy as sa
from sqlalchemy.orm import Session
from sqlalchemy.sql import func

from .base import Base, Record


class UserRecord(Record):
    email: str
    hashed_password: str

    class Config:
        orm_mode = True


class UserSchema(Base):
    __tablename__ = "users"

    __table_args__ = (
        sa.Index("email_uniq_idx", "email", "not_deleted", unique=True),
        sa.Index(
            "email_disabled_idx",
            "email",
            "disabled",
            "not_deleted",
        ),
        {
            "mysql_engine": "InnoDB",
            "mysql_collate": "utf8mb4_unicode_ci",
            "mysql_charset": "utf8mb4",
        },
    )

    id = sa.Column(
        sa.BigInteger,
        primary_key=True,
    )

    created_at = sa.Column(
        sa.DateTime(timezone=True),
        nullable=False,
        default=func.now(),
    )

    updated_at = sa.Column(
        sa.DateTime(timezone=True),
        nullable=False,
        default=func.now(),
        onupdate=func.now(),
    )

    deleted_at = sa.Column(
        sa.DateTime(timezone=True),
        nullable=True,
        default=None,
    )

    not_deleted = sa.Column(
        sa.Boolean,
        sa.Computed(sqltext="IF(deleted_at IS NULL, 1, NULL)", persisted=False),
        nullable=True,
    )

    email = sa.Column(
        sa.String(length=512),
        nullable=False,
    )

    hashed_password = sa.Column(
        sa.BINARY(60),
        nullable=False,
    )

    disabled = sa.Column(
        sa.Boolean,
        default=False,
        nullable=False,
    )

    def __init__(self, email: str, password: str):
        self.email = email
        self.hashed_password = bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt())

    def change_password(self, password):
        self.hashed_password = bcrypt.hashpw(password.encode("utf-8"), self.hashed_password)

    def verify_password(self, password):
        return bcrypt.checkpw(password.encode("utf-8"), self.hashed_password)

class UserRepository:
    @staticmethod
    def create(db: Session, record: UserSchema) -> UserSchema:
        db.add(record)
        db.commit()
        db.refresh(record)
        return record
    @staticmethod
    def get_by_id(db: Session, id: int) -> UserSchema:
        record = db.query(UserSchema).filter(UserSchema.id == id).filter(UserSchema.not_deleted==True).first()
        return record
    @staticmethod
    def get_by_email(db: Session, email: str) -> UserSchema:
        record = db.query(UserSchema).filter(UserSchema.email == email).filter(UserSchema.not_deleted==True).first()
        return record
    @staticmethod
    def update_password(db: Session, record: UserSchema, password: str) -> UserSchema:
        record.change_password(password=password)
        db.commit()
        return record
