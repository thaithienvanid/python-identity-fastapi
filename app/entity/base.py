from datetime import datetime

from pydantic import BaseModel
from sqlalchemy.orm import declarative_base

Base = declarative_base()

class Model(BaseModel):
    id: int
    created_at: datetime
    updated_at: datetime


class Record(Model):
    deleted_at: datetime | None
    not_deleted: bool | None
