from datetime import datetime
from typing import List

from fastapi import APIRouter
from pydantic import BaseModel

router = APIRouter()


class Pong(BaseModel):
    """
    Pong is a response model for GET /ping
    """

    timestamp: int


@router.get("/ping/", response_model=Pong)
def pong():
    return Pong(timestamp=int(datetime.utcnow().timestamp() * 1000))
