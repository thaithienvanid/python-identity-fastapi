from fastapi.security import HTTPBearer
from jose import jwt
from jose.backends.base import Key
from jose.constants import ALGORITHMS

from app.database import SessionLocal
from app.security import token_key


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


def sign(key: Key, data: dict):
    return jwt.encode(claims=data, key=key, algorithm=ALGORITHMS.ES256)


def verify(key: Key, token: str):
    payload = jwt.decode(token, key=key, algorithms=ALGORITHMS.ES256)
    return payload


reusable_oauth2 = HTTPBearer(scheme_name="Authorization")
