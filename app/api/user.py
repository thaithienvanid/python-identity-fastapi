from datetime import datetime, timedelta

from fastapi import APIRouter, Depends, HTTPException
from jose import JWTError
from pydantic import BaseModel, EmailStr, Field
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError

from app.entity.base import Model
from app.entity.user import UserSchema, UserRepository
from .tool import get_db, token_key, sign, verify, reusable_oauth2


router = APIRouter()


class UserCreateInput(BaseModel):
    email: EmailStr
    password: str = Field(..., min_length=8, max_length=72, strip_whitespace=True)


class UserResponse(Model):
    email: str

    class Config:
        orm_mode = True


@router.post("/user/", response_model=UserResponse)
def create_user(*, db: Session = Depends(get_db), payload: UserCreateInput):
    record = UserSchema(email=payload.email, password=payload.password)
    try:
        return UserRepository.create(db, record)
    except IntegrityError as e:
        raise HTTPException(status_code=400, detail=str(e.orig))


class UserTokenInput(BaseModel):
    email: EmailStr
    password: str = Field(..., min_length=8, max_length=72, strip_whitespace=True)


class UserTokenResponse(BaseModel):
    access_token: str


class UserTokenClaims(BaseModel):
    sub: str
    exp: datetime
    user_id: int


@router.post("/user/token", response_model=UserTokenResponse)
def create_user_token(*, db: Session = Depends(get_db), payload: UserTokenInput):
    record = UserRepository.get_by_email(db, payload.email)
    if not record:
        raise HTTPException(status_code=404, detail="user not found")
    if not record.verify_password(payload.password):
        raise HTTPException(status_code=401, detail="password not valid")
    claims = UserTokenClaims(
        sub=str(record.id),
        exp=datetime.utcnow() + timedelta(minutes=15),
        user_id=record.id,
    )
    token = sign(token_key, claims.dict())
    return UserTokenResponse(access_token=token)


class UserPasswordUpdateInput(BaseModel):
    email: EmailStr
    old_password: str = Field(..., min_length=8, max_length=72, strip_whitespace=True)
    new_password: str = Field(..., min_length=8, max_length=72, strip_whitespace=True)


@router.get("/user/me", response_model=UserResponse)
def get_me(*, db: Session = Depends(get_db), authorization=Depends(reusable_oauth2)):
    try:
        payload = verify(token_key.public_key(), authorization.credentials)
        claims = UserTokenClaims(**payload)
        record = UserRepository.get_by_id(db, claims.user_id)
        if not record:
            raise HTTPException(status_code=403, detail="token not valid")
        return record
    except JWTError as e:
        raise HTTPException(status_code=403, detail="token not valid")


@router.put("/user/password", response_model=UserResponse)
def update_user_password(
    *, db: Session = Depends(get_db), payload: UserPasswordUpdateInput
):
    record = UserRepository.get_by_email(db, payload.email)
    if not record:
        raise HTTPException(status_code=404, detail="user not found")
    if not record.verify_password(payload.old_password):
        raise HTTPException(status_code=401, detail="password not valid")
    record = UserRepository.update_password(db, record, payload.new_password)
    return record
