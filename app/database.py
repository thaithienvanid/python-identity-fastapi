from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.config import settings

engine = create_engine(settings.db.dsn, echo=settings.is_dev_env())
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
