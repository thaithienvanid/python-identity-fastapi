from functools import lru_cache
from pydantic import BaseModel, BaseSettings


DEVELOPMENT = "dev"
PRODUCTION = "prod"


class DatabaseSettings(BaseModel):
    dsn: str


class SecuritySettings(BaseModel):
    key: str


class Settings(BaseSettings):
    env: str = DEVELOPMENT
    host: str = "0.0.0.0"
    port: int = 5000

    db: DatabaseSettings
    sec: SecuritySettings

    class Config:
        env_file = ".env", ".env.prod"
        env_file_encoding = "utf-8"
        env_nested_delimiter = "__"

    def is_dev_env(self) -> bool:
        return True if self.env == DEVELOPMENT else False


@lru_cache()
def get_settings():
    return Settings()


settings = get_settings()
