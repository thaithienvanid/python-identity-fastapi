from fastapi import FastAPI

from app.api import ping
from app.api import user

app = FastAPI(
    title="identity",
)
app.include_router(ping.router)
app.include_router(user.router)
