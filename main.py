import uvicorn

from app.config import settings
from app.database import engine
from app.entity.user import UserSchema

UserSchema.metadata.create_all(engine)

if __name__ == "__main__":
    uvicorn.run(
        "app.app:app",
        host=settings.host,
        port=settings.port,
        reload=settings.is_dev_env(),
        workers=8,
        access_log=False,
    )
