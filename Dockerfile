FROM python:3.10-alpine

RUN apk add --update --no-cache ca-certificates curl poetry tzdata
RUN ln -fs /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime

RUN apk add --update --no-cache gcc musl-dev python3-dev libffi-dev openssl-dev

WORKDIR /repo

COPY pyproject.toml poetry.lock /repo/
RUN poetry install --only=main

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY app /repo/app
COPY main.py /repo/

CMD ["poetry", "run", "python", "main.py"]
