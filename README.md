# Identity

This project is an Identity service implemented in Python

## Prerequisites

- docker & docker compose (v2)
- python3
- poetry

  ```sh
  pip3 install poetry
  ```

## Develop

- Install dependencies

  ```sh
  poetry install
  ```

- Run infrastructure

  ```sh
  docker compose -f docker-compose.infra.yml up -d
  ```

- Run application

  ```sh
  poetry run python main.py
  ```

- API & Playground
  - See more at [Redoc](http://localhost:5000/redoc)
  - See more at [OpenAPI](http://localhost:5000/docs)

## Testing

- Run testing

  ```sh
  poetry run pytest --cov=app tests/
  ```

  ```sh
  ---------- coverage: platform darwin, python 3.10.9-final-0 ----------
  Name                     Stmts   Miss  Cover
  --------------------------------------------
  app/__init__.py              0      0   100%
  app/api/__init__.py          0      0   100%
  app/api/ping.py             10      0   100%
  app/api/tool.py             17      0   100%
  app/api/user.py             70      0   100%
  app/app.py                   6      0   100%
  app/config.py               24      0   100%
  app/database.py              5      0   100%
  app/entity/__init__.py       0      0   100%
  app/entity/base.py          11      0   100%
  app/entity/user.py          48      0   100%
  app/security.py              4      0   100%
  --------------------------------------------
  TOTAL                      195      0   100%
  ```

- Coverage: 100%

## Deployment

- Local

  ```sh
  docker compose -f docker-compose.yml up -d --build
  ```
