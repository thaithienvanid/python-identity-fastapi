from datetime import datetime, timedelta

from jose import jwk
from sqlalchemy.orm import Session

from app.api.tool import get_db, sign, verify


def test_get_db():
    g = get_db()
    db = g.send(None)
    assert db is not None
    assert isinstance(db, Session)


def test_sign_verify():
    test_key_raw = {
        "use": "sig",
        "kty": "EC",
        "kid": "-MYtdG0cF3hABXUVIzgigR0mI1nkNWDit7eSfT3eEYg=",
        "crv": "P-256",
        "alg": "ES256",
        "x": "-2hwDQhmk5ulFy4akAAIqGTpM-95SK4TBDhMvbLnz0Q",
        "y": "-x8MFBz2eTs1bqOOPoWj_uhTKkGCrB6CaHIjsb_JrbE",
        "d": "7iyrUnvn5NNXMwzd4lMx2PcVc9QSSt06c8lyimCeuTw",
    }
    key = jwk.construct(test_key_raw)
    
    data = {"sub": "1", "exp": datetime.utcnow() + timedelta(minutes=15)}

    token = sign(key, data)
    assert token is not None

    payload = verify(key.public_key(), token)
    assert payload is not None
    assert payload == data
