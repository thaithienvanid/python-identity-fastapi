from datetime import datetime, timedelta
import json

from sqlalchemy.exc import IntegrityError

from app.entity.user import UserSchema, UserRepository


def test_create_user(test_app, monkeypatch):
    email = "test@example.com"
    password = "password"
    data = {"email": email, "password": password}
    user = UserSchema(email, password)

    def mock_create(*args, **kwargs):
        user.id = 1
        user.created_at = datetime.utcnow()
        user.updated_at = datetime.utcnow()
        user.not_deleted = True
        return user

    monkeypatch.setattr(UserRepository, "create", mock_create)

    response = test_app.post(
        "/user/",
        content=json.dumps(data),
    )
    assert response.status_code == 200
    assert response.json()["id"] > 0
    assert response.json()["email"] == user.email


def test_create_user_failure(test_app, monkeypatch):
    email = "test@example.com"
    password = "password"
    data = {"email": email, "password": password}

    def mock_create(*args, **kwargs):
        raise IntegrityError(statement="test", params="test", orig="test")

    monkeypatch.setattr(UserRepository, "create", mock_create)

    response = test_app.post(
        "/user/",
        content=json.dumps(data),
    )
    assert response.status_code == 400


def test_create_user_token(test_app, monkeypatch):
    email = "test@example.com"
    password = "password"
    data = {"email": email, "password": password}
    user = UserSchema(email, password)

    def mock_get_by_email(*args, **kwargs):
        user.id = 1
        user.created_at = datetime.utcnow()
        user.updated_at = datetime.utcnow()
        user.not_deleted = True
        return user

    monkeypatch.setattr(UserRepository, "get_by_email", mock_get_by_email)

    response = test_app.post(
        "/user/token",
        content=json.dumps(data),
    )
    assert response.status_code == 200
    assert response.json()["access_token"] is not None


def test_create_user_token_invalid_email(test_app, monkeypatch):
    email = "test@example.com"
    password = "password"
    data = {"email": email, "password": password}

    def mock_get_by_email(*args, **kwargs):
        return None

    monkeypatch.setattr(UserRepository, "get_by_email", mock_get_by_email)

    response = test_app.post(
        "/user/token",
        content=json.dumps(data),
    )
    assert response.status_code == 404


def test_create_user_token_invalid_password(test_app, monkeypatch):
    email = "test@example.com"
    password = "password"
    fakepassword = "fakepassword"
    data = {"email": email, "password": fakepassword}
    user = UserSchema(email, password)

    def mock_get_by_email(*args, **kwargs):
        user.id = 1
        user.created_at = datetime.utcnow()
        user.updated_at = datetime.utcnow()
        user.not_deleted = True
        return user

    monkeypatch.setattr(UserRepository, "get_by_email", mock_get_by_email)

    response = test_app.post(
        "/user/token",
        content=json.dumps(data),
    )
    assert response.status_code == 401


def test_get_user_me(test_app, monkeypatch):
    email = "test@example.com"
    password = "password"
    data = {"email": email, "password": password}
    user = UserSchema(email, password)

    def mock_get_user(*args, **kwargs):
        user.id = 1
        user.created_at = datetime.utcnow()
        user.updated_at = datetime.utcnow()
        user.not_deleted = True
        return user

    monkeypatch.setattr(UserRepository, "get_by_email", mock_get_user)

    response = test_app.post(
        "/user/token",
        content=json.dumps(data),
    )
    assert response.status_code == 200

    access_token = response.json()["access_token"]

    monkeypatch.setattr(UserRepository, "get_by_id", mock_get_user)

    response = test_app.get(
        "/user/me", headers={"Authorization": "Bearer " + access_token}
    )
    assert response.status_code == 200
    assert response.json()["id"] > 0
    assert response.json()["email"] == user.email


def test_get_user_me_invalid_user(test_app, monkeypatch):
    email = "test@example.com"
    password = "password"
    data = {"email": email, "password": password}
    user = UserSchema(email, password)

    def mock_get_by_email(*args, **kwargs):
        user.id = 1
        user.created_at = datetime.utcnow()
        user.updated_at = datetime.utcnow()
        user.not_deleted = True
        return user

    monkeypatch.setattr(UserRepository, "get_by_email", mock_get_by_email)

    response = test_app.post(
        "/user/token",
        content=json.dumps(data),
    )
    assert response.status_code == 200

    access_token = response.json()["access_token"]

    def mock_get_by_id(*args, **kwargs):
        return None

    monkeypatch.setattr(UserRepository, "get_by_id", mock_get_by_id)

    response = test_app.get(
        "/user/me", headers={"Authorization": "Bearer " + access_token}
    )
    assert response.status_code == 403


def test_get_user_me_invalid_token(test_app, monkeypatch):
    access_token = "fake"
    response = test_app.get(
        "/user/me", headers={"Authorization": "Bearer " + access_token}
    )
    assert response.status_code == 403


def test_update_user_password(test_app, monkeypatch):
    email = "test@example.com"
    old_password = "old_password"
    new_password = "new_password"
    data = {"email": email, "old_password": old_password, "new_password": new_password}
    user = UserSchema(email, old_password)

    def mock_get_by_email(*args, **kwargs):
        user.id = 1
        user.created_at = datetime.utcnow()
        user.updated_at = datetime.utcnow()
        user.not_deleted = True
        return user

    monkeypatch.setattr(UserRepository, "get_by_email", mock_get_by_email)

    def mock_update_password(*args, **kwargs):
        user.change_password(new_password)
        assert user.verify_password(new_password) is True
        return user

    monkeypatch.setattr(UserRepository, "update_password", mock_update_password)

    response = test_app.put(
        "/user/password",
        content=json.dumps(data),
    )
    assert response.status_code == 200
    assert response.json()["id"] > 0
    assert response.json()["email"] == user.email


def test_update_user_password_invalid_email(test_app, monkeypatch):
    email = "test@example.com"
    old_password = "old_password"
    new_password = "new_password"
    data = {"email": email, "old_password": old_password, "new_password": new_password}

    def mock_get_by_email(*args, **kwargs):
        return None

    monkeypatch.setattr(UserRepository, "get_by_email", mock_get_by_email)

    response = test_app.put(
        "/user/password",
        content=json.dumps(data),
    )
    assert response.status_code == 404


def test_update_user_password_invalid_password(test_app, monkeypatch):
    email = "test@example.com"
    password = "password"
    old_password = "old_password"
    new_password = "new_password"
    data = {"email": email, "old_password": old_password, "new_password": new_password}
    user = UserSchema(email, password)

    def mock_get_by_email(*args, **kwargs):
        user.id = 1
        user.created_at = datetime.utcnow()
        user.updated_at = datetime.utcnow()
        user.not_deleted = True
        return user

    monkeypatch.setattr(UserRepository, "get_by_email", mock_get_by_email)

    response = test_app.put(
        "/user/password",
        content=json.dumps(data),
    )
    assert response.status_code == 401
