from sqlalchemy.orm import Session, Query
from app.entity.user import UserSchema, UserRepository


def test_user_schema():
    email = "test@example.com"
    password = "password"
    newpassword = "newpassword"
    fakepassword = "fakepassword"

    user = UserSchema(email, password)
    assert user is not None
    assert user.email == email
    assert user.verify_password(password) == True
    assert user.verify_password(fakepassword) == False

    user.change_password(password=newpassword)
    assert user.verify_password(newpassword) == True
    assert user.verify_password(fakepassword) == False


def test_user_repository(monkeypatch):
    id = 1
    email = "test@example.com"
    password = "password"
    newpassword = "newpassword"
    record = UserSchema(email, password)
    record.id = 1

    def nothing(*args, **kwargs):
        pass

    db = Session()

    monkeypatch.setattr(db, "add", nothing)
    monkeypatch.setattr(db, "commit", nothing)
    monkeypatch.setattr(db, "refresh", nothing)

    actual = UserRepository.create(db, record)
    assert actual.email == record.email
    assert actual.hashed_password == record.hashed_password

    query = db.query(UserSchema)

    def mock_query(*args, **kwargs) -> Query:
        return query

    monkeypatch.setattr(db, "query", mock_query)
    monkeypatch.setattr(query, "filter", mock_query)

    def mock_user(*args, **kwargs) -> UserSchema:
        return record

    monkeypatch.setattr(query, "first", mock_user)

    actual = UserRepository.get_by_id(db, id)
    assert actual.id == record.id

    actual = UserRepository.get_by_email(db, email)
    assert actual.email == record.email

    assert actual.verify_password(password) == True
    actual = UserRepository.update_password(db, record, newpassword)
    assert actual.email == record.email
    assert actual.verify_password(newpassword) == True
